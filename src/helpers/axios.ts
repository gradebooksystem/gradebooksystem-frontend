import axios from "axios";

axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
axios.defaults.headers.common["X-Cache-Control"] = "no-cache";

export const axiosInstance = axios.create({
  baseURL: "http://localhost:8080/api/"
});
