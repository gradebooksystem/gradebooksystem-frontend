import { RecordBookTableItem, RecordBook } from "./reducer";
import { axiosInstance } from "helpers/axios";
import {
  SET_VALUES,
  SET_LOADING_STATE,
  SET_CURRENT_RECORD_BOOK
} from "./actionTypes";
import { message } from "antd";
import { history } from "helpers/history";

export const setValues = (values: Array<RecordBookTableItem>) => ({
  type: SET_VALUES,
  payload: values
});

export const setLoadingState = (loadingState: Boolean) => ({
  type: SET_LOADING_STATE,
  payload: loadingState
});

export const setCurrentRecordBook = (book: RecordBook | any) => ({
  type: SET_CURRENT_RECORD_BOOK,
  payload: book
});

export const createRecordBook = (object: any) => async (dispatch: any) => {
  try {
    axiosInstance.post("record-books/", object);
  } catch (e) {
    message.error("Something went wrong.");
  }
  message.success("Successfully created");
  history.push("/recordbooks");
};

export const getRecordBooks = () => async (dispatch: any) => {
  dispatch(setLoadingState(true));

  const response = await axiosInstance.get("record-books");
  dispatch(setValues(response.data));
  dispatch(setLoadingState(false));
};
export const getRecordBook = (id: Number) => async (dispatch: any) => {
  dispatch(setLoadingState(true));

  const response = await axiosInstance.get(`record-books/${id}`);
  dispatch(setCurrentRecordBook(response.data));
  dispatch(setLoadingState(false));
};
