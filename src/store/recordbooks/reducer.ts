import {
  SET_LOADING_STATE,
  SET_VALUES,
  SET_CURRENT_RECORD_BOOK
} from "./actionTypes";

export interface RecordBook {
  id?: number;
  name?: string;
  groupId?: number;
  code?: string;
  entries: Array<RecordBookEntry>;
}
export interface RecordBookEntry {
  uid: string;
  subjectName?: string;
  semester: number;
  grade?: string;
  teacherName?: string;
  date: string;
}

export interface RecordBookTableItem {
  id: number;
  name: string;
  group: string;
}

export interface RecordBookItem {
  id: number;
  semester: number;
  date: string;
  grade: number;
  subjectName: string;
}

export interface RecordBooksReduxState {
  current?: RecordBook;
  values: Array<RecordBookTableItem>;
  loading: Boolean;
}

const initialState: RecordBooksReduxState = {
  current: undefined,
  values: [],
  loading: false
};
export interface ActionType {
  type: string;
  payload: any;
}

interface RecordBookHadlerType {
  [property: string]: (
    state: RecordBooksReduxState,
    action: ActionType
  ) => RecordBooksReduxState;
}

const RecordBookHandler: RecordBookHadlerType = {
  [SET_LOADING_STATE](state: RecordBooksReduxState, action: ActionType) {
    return { ...state, loading: action.payload };
  },
  [SET_VALUES](state: RecordBooksReduxState, action: ActionType) {
    return { ...state, values: action.payload };
  },
  [SET_CURRENT_RECORD_BOOK](state: RecordBooksReduxState, action: ActionType) {
    return { ...state, current: action.payload };
  }
};

export default (state = initialState, action: any) =>
  RecordBookHandler[action.type]
    ? RecordBookHandler[action.type](state, action)
    : state;
