export interface StandardComponentProps {
  title?: string;
  children: React.ReactNode;
}
