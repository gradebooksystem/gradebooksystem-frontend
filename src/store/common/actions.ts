import { setLoadingState } from "store/recordbooks/actions";
import { axiosInstance } from "helpers/axios";
import { SET_FORM_DATA } from "./actionTypes";

export const exportFromFile = (fileContent: any) => {};
export const setFormData = (formData: any) => ({
  type: SET_FORM_DATA,
  payload: formData
});

export const getFormData = () => async (dispatch: any) => {
  dispatch(setLoadingState(true));
  try {
    const subjects = axiosInstance.get("subjects");
    const teachers = axiosInstance.get("teachers");
    const groups = axiosInstance.get("groups");
    const responses = await Promise.all([teachers, subjects, groups]);
    dispatch(
      setFormData({
        teachers: responses[0].data,
        subjects: responses[1].data,
        groups: responses[2].data
      })
    );
  } catch (e) {
    console.log(e.message);
  } finally {
    dispatch(setLoadingState(false));
  }
};
