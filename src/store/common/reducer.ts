import { SET_FORM_DATA } from "./actionTypes";
import { ActionType } from "store/recordbooks/reducer";

const initialState = {
  teachers: [],
  groups: [],
  subjects: []
};

export interface Data {
  id: number;
  name: string;
}

export interface CommonDataState {
  teachers: Array<Data>;
  groups: Array<Data>;
  subjects: Array<Data>;
}
export interface CommonDataHandlerType {
  [property: string]: (
    state: CommonDataState,
    action: ActionType
  ) => CommonDataState;
}
const CommonDataHandler: CommonDataHandlerType = {
  [SET_FORM_DATA](state: any, action: ActionType) {
    return { ...state, ...action.payload };
  }
};

export default (state: CommonDataState = initialState, action: ActionType) =>
  CommonDataHandler[action.type]
    ? CommonDataHandler[action.type](state, action)
    : state;
