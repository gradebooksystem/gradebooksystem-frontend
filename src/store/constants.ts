import { RecordBookEntry } from "./recordbooks/reducer";
import moment from "moment";

export const isNumeric = (n: any) => {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

export const DATE_FORMAT = "YYYY/MM/DD";
export const defaultColumnProperty = { span: 20, sm: 5, lg: 4 };
export const getDefaultRecordBookEntry = () => {
  const defaultRecordBookEntry: RecordBookEntry = {
    uid: "",
    semester: 0,
    date: moment().format(DATE_FORMAT),
    grade: undefined,
    subjectName: undefined,
    teacherName: undefined
  };

  return { ...defaultRecordBookEntry };
};

export const parseEntriesValues = (entries: any, size: number) => {
  let obj = [];
  for (let i = 0; i < size; i++) {
    let entity: RecordBookEntry = {
      uid: `entry-${i}`,
      semester: 0,
      date: moment().format()
    };
    entity["subjectName"] = entries[`subjectName-${i}`];
    entity["semester"] = entries[`semester-${i}`];
    entity["grade"] = entries[`grade-${i}`];
    entity["teacherName"] = entries[`treacher-${i}`];
    entity["date"] = moment(entries[`date-${i}`]).format(DATE_FORMAT);
    obj.push(entity);
  }
  return obj;
};

export const sidebarItems = [
  {
    name: "RecordBooks",
    linkTo: "/recordbooks",
    icon: "book"
  },
  {
    name: "Gradebooks",
    linkTo: "/gradebooks",
    icon: "account-book"
  }
];
