import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createLogger } from "redux-logger";
import recordbooks from "store/recordbooks/reducer";
import commonDataReducer from "store/common/reducer";
import thunk from "redux-thunk";

const logger = createLogger({
  collapsed: true
});

export const store = createStore(
  combineReducers({ recordbooks: recordbooks, common: commonDataReducer }),
  composeWithDevTools(applyMiddleware(thunk, logger))
);
console.log(store);
