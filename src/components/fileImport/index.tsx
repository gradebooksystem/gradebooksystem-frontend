import { StandardComponentProps } from "store/commonTypes";
import React, { FormEvent } from "react";

import { RecordBook } from "store/recordbooks/reducer";
import {
  Card,
  PageHeader,
  Spin,
  Form,
  Row,
  Upload,
  Button,
  Icon,
  Col
} from "antd";
import { history } from "helpers/history";
import { WrappedFormUtils, FormComponentProps } from "antd/lib/form/Form";

interface RecordBookFormProps extends FormComponentProps {
  form: WrappedFormUtils;
  current: RecordBook;

  loading: boolean | undefined;
  getRecordBook: (id: Number) => void;
  getFormData: () => void;
  setCurrentBook: (book: RecordBook) => void;
}

class FileImportForm extends React.Component<StandardComponentProps> {
  state = {
    file: undefined,
    uploading: false
  };

  handleUpload = (f: any) => {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      console.log(e.target.result);
    };
    reader.readAsText(f);

    this.setState({
      uploading: true
    });

    // You can use any AJAX library you like
    return false;
  };

  async componentDidMount() {}

  render() {
    console.log("props", this.props);
    return (
      <div>
        <div>
          <PageHeader
            title="Import from File"
            onBack={() => history.go(-1)}
          ></PageHeader>
        </div>
        <Card className="entity-table">{this.renderForm()}</Card>
      </div>
    );
  }

  submit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };

  renderForm() {
    return (
      <Spin spinning={false}>
        <Form onSubmit={this.submit}>
          <Row gutter={10} type="flex" className="form_header_ro">
            <Col>
              <p>Insert file to automaticly add data</p>
            </Col>
          </Row>
          <Row gutter={10} type="flex" className="form_header_ro">
            <Col>
              <Upload beforeUpload={this.handleUpload}>
                <Button>
                  <Icon type="upload" /> Click to Upload
                </Button>
              </Upload>
            </Col>
          </Row>
        </Form>
      </Spin>
    );
  }
}

export default Form.create<RecordBookFormProps & StandardComponentProps>()(
  FileImportForm
);
