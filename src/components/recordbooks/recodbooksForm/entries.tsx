import React from "react";
import { StandardComponentProps } from "store/commonTypes";
import { RecordBookEntry } from "store/recordbooks/reducer";
import { Form } from "antd";
import { WrappedFormUtils, FormComponentProps } from "antd/lib/form/Form";
import RecordBookEntryComponent from "./entry";
import { CommonDataState } from "store/common/reducer";

interface RecordBookEntriesProps extends FormComponentProps {
  entries: Array<RecordBookEntry>;
  form: WrappedFormUtils;
  data: CommonDataState;
  removeEntry: (id: string) => void;
}

class RecordBookEntries extends React.Component<
  RecordBookEntriesProps & StandardComponentProps
> {
  public entriesRef: Array<any> = [];
  render() {
    const { form, entries, removeEntry, data } = this.props;

    const formItems = entries.map((entry, index) => (
      <RecordBookEntryComponent
        wrappedComponentRef={(ref: any) => this.entriesRef.push(ref)}
        form={form}
        data={data}
        index={index}
        entry={entry}
        removeEntry={removeEntry}
      />
    ));

    return <div>{formItems}</div>;
  }
}

export default Form.create<RecordBookEntriesProps>()(RecordBookEntries);
