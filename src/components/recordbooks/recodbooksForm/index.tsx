import { StandardComponentProps } from "store/commonTypes";
import React, { FormEvent } from "react";
import RecordBookEntries from "./entries";
import { RecordBook } from "store/recordbooks/reducer";
import {
  Card,
  PageHeader,
  Button,
  Spin,
  Form,
  Input,
  Row,
  Col,
  Select
} from "antd";
import { history } from "helpers/history";
import { WrappedFormUtils, FormComponentProps } from "antd/lib/form/Form";
import {
  defaultColumnProperty,
  getDefaultRecordBookEntry,
  isNumeric,
  parseEntriesValues
} from "store/constants";
import { Data, CommonDataState } from "store/common/reducer";
var uuid = require("uuid/v4");
interface RecordBookFormProps extends FormComponentProps {
  form: WrappedFormUtils;
  current: RecordBook;
  data: CommonDataState;
  match: any;
  loading: boolean | undefined;
  create: (obj: any) => void;
  update: (obj: any) => void;
  getRecordBook: (id: Number) => void;
  getFormData: () => void;
  setCurrentBook: (book: RecordBook) => void;
}

class RecordBookForm extends React.Component<
  RecordBookFormProps & StandardComponentProps
> {
  private entries?: any;

  async componentDidMount() {
    const { getRecordBook, getFormData, setCurrentBook } = this.props;
    await getFormData();
    const actionOrId = this.props.match.params.actionOrId;
    if (isNumeric(actionOrId)) getRecordBook(+actionOrId);
    else {
      let recordBook: RecordBook = {
        entries: [getDefaultRecordBookEntry()],
        code: undefined,
        groupId: undefined,
        id: undefined,
        name: undefined
      };
      setCurrentBook(recordBook);
    }
  }

  addNewEntry = () => {
    const { setCurrentBook, current } = this.props;
    let entry = getDefaultRecordBookEntry();
    entry.uid = uuid();
    setCurrentBook({
      ...current,
      entries: [...current.entries, entry]
    });
  };
  removeEntry = (id: string) => {
    const { current, setCurrentBook } = this.props;
    setCurrentBook({
      ...current,
      entries: current.entries.filter(el => el.uid !== id)
    });
  };

  render() {
    return (
      <div>
        <div>
          <PageHeader
            title="RecordBook"
            onBack={() => history.go(-1)}
          ></PageHeader>
        </div>
        <Card className="entity-table">{this.renderForm()}</Card>
      </div>
    );
  }

  submit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const { form, current, create, update } = this.props;

    let error1 = false;
    form.validateFields(errors => {
      if (errors) error1 = true;
    });
    if (!this.entries) return;

    this.entries.props.form.validateFields((errors: any) => {
      if (errors) error1 = true;
    });
    if (error1) return;

    let result = form.getFieldsValue();
    result.entries = parseEntriesValues(
      this.entries.props.form.getFieldsValue(),
      current.entries.length
    );
    console.log(result);
    const actionOrId = this.props.match.params.actionOrId;
    if (isNumeric(actionOrId)) create(result);
    else update(result);
  };

  renderForm() {
    let { form, loading, current, data } = this.props;
    const { getFieldDecorator } = form;
    current = current || {};
    return (
      <Spin spinning={loading}>
        <Form onSubmit={this.submit}>
          <Row gutter={10} type="flex" className="form_header_ro">
            <Col {...defaultColumnProperty} lg={6}>
              <Form.Item label="Student name">
                {getFieldDecorator("name", {
                  rules: [{ required: true, message: "Name is Required" }],
                  initialValue: current.name
                })(<Input placeholder="Student Name" />)}
              </Form.Item>
            </Col>
            <Col {...defaultColumnProperty}>
              <Form.Item label="Code">
                {getFieldDecorator("code", {
                  rules: [{ required: true, message: "Code is Required" }],
                  initialValue: current.code
                })(<Input placeholder="Record Book Code" />)}
              </Form.Item>
            </Col>
            <Col {...defaultColumnProperty}>
              <Form.Item label="Group">
                {getFieldDecorator("group", {
                  rules: [{ required: true, message: "group is Required" }],
                  initialValue: current.groupId
                })(
                  <Select placeholder="Studen Group">
                    {data.groups.map((el: Data) => (
                      <Select.Option key={el.id}>{el.name}</Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col {...defaultColumnProperty}>
              <Form.Item label="        ">
                <Button onClick={this.addNewEntry}>+ Add New Entry</Button>
              </Form.Item>
            </Col>
            <Col {...defaultColumnProperty}>
              <Button
                type="primary"
                style={{ marginTop: "43px" }}
                htmlType="submit"
              >
                Save
              </Button>
            </Col>
          </Row>
          <RecordBookEntries
            wrappedComponentRef={(ref: any) => (this.entries = ref)}
            entries={current.entries || []}
            data={data}
            removeEntry={this.removeEntry}
          />
        </Form>
      </Spin>
    );
  }
}

export default Form.create<RecordBookFormProps & StandardComponentProps>()(
  RecordBookForm
);
