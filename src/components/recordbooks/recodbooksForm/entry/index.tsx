import { RecordBookEntry } from "store/recordbooks/reducer";
import { WrappedFormUtils } from "antd/lib/form/Form";
import React from "react";
import { Row, Col, Form, Input, DatePicker, Button, Icon, Select } from "antd";
import { defaultColumnProperty, DATE_FORMAT } from "store/constants";
import moment from "moment";
import { CommonDataState } from "store/common/reducer";

interface RecordBookEntryProp {
  entry: RecordBookEntry;
  index: number;
  data: CommonDataState;
  form: WrappedFormUtils;
  wrappedComponentRef: (ref: any) => void;
  removeEntry: (id: string) => void;
}

export default class RecordBookEntryComponent extends React.Component<
  RecordBookEntryProp & { form: WrappedFormUtils }
> {
  render() {
    const { form, index, entry, data } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Row gutter={10} key={`paymentWrap-${index}`} className="payment-wrap">
        <Col {...defaultColumnProperty}>
          <Form.Item
            label={index === 0 && "Subject"}
            key={`description-${index}`}
          >
            {getFieldDecorator(`subjectName-${index}`, {
              rules: [{ required: true, message: "Subject is required" }],
              initialValue: entry.subjectName
            })(
              <Select placeholder="Subject">
                {data.subjects.map(el => (
                  <Select.Option key={el.id}>{el.name}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col {...defaultColumnProperty}>
          <Form.Item
            label={index === 0 && "Semester"}
            key={`semester-${index}`}
          >
            {getFieldDecorator(`semester-${index}`, {
              rules: [{ required: true, message: "Semester is required" }],
              initialValue: entry.semester
            })(<Input placeholder="semester" />)}
          </Form.Item>
        </Col>

        <Col {...defaultColumnProperty}>
          <Form.Item label={index === 0 && "Grade"} key={`grade-${index}`}>
            {getFieldDecorator(`grade-${index}`, {
              rules: [{ required: true, message: "Grade is required" }],
              initialValue: entry.grade
            })(<Input placeholder="Grade" />)}
          </Form.Item>
        </Col>
        <Col {...defaultColumnProperty}>
          <Form.Item label={index === 0 && "Teacher"} key={`teacher-${index}`}>
            {getFieldDecorator(`teacher-${index}`, {
              rules: [{ required: true, message: "Teacher is required" }],
              initialValue: entry.teacherName
            })(
              <Select placeholder="Teacher">
                {data.teachers.map(el => (
                  <Select.Option key={el.id}>{el.name}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col {...defaultColumnProperty} lg={5}>
          <Form.Item label={index === 0 && "Date"} key={`date-${index}`}>
            {getFieldDecorator(`date-${index}`, {
              rules: [{ required: true, message: "Date is required" }],
              initialValue: moment(entry.date)
            })(<DatePicker placeholder="Date" format={DATE_FORMAT} />)}
          </Form.Item>
        </Col>
        <Col {...defaultColumnProperty} lg={2}>
          <Form.Item
            label={index === 0 && "Remove"}
            key={`remove-btn-${index}`}
          >
            <Button
              type="danger"
              onClick={() => {
                this.props.removeEntry(entry.uid);
              }}
            >
              <Icon type="delete" />
            </Button>
          </Form.Item>
        </Col>
      </Row>
    );
  }
}
