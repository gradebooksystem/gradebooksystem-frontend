import { StandardComponentProps } from "store/commonTypes";
import React from "react";
import { RecordBookTableItem } from "store/recordbooks/reducer";
import { Table, Card, PageHeader, Button } from "antd";
import { history } from "helpers/history";
import { Link } from "react-router-dom";

type RecordBookTableProps = {
  values: Array<RecordBookTableItem>;
  loading: Boolean;
  getRecordBooks: () => void;
};

export class RecordBookTable extends React.Component<
  RecordBookTableProps & StandardComponentProps
> {
  componentDidMount() {
    const { getRecordBooks } = this.props;
    getRecordBooks();
  }

  render() {
    const { values } = this.props;
    const columns = [
      {
        title: "Code",
        dataIndex: "code",
        key: "code"
      },
      {
        title: "Student name",
        render: (text: string, record: RecordBookTableItem) => (
          <Link to={`recordbooks/${record.id}`}>{record.name}</Link>
        ),
        key: "studentname"
      },
      {
        title: "Group",
        dataIndex: "group",
        key: "group"
      }
    ];
    return (
      <div>
        <div>
          <PageHeader
            title="RecordBooks"
            onBack={() => null}
            extra={[
              <Button
                onClick={() => history.push("/recordbooks/new")}
                type="primary"
              >
                + Add New Record Book
              </Button>,
              <Button
                onClick={() => history.push("/file-import")}
                type="default"
              >
                + Import From File
              </Button>
            ]}
          ></PageHeader>
        </div>
        <Card className="entity-table">
          <Table columns={columns} dataSource={values} />;
        </Card>
      </div>
    );
  }
}
