import { StandardComponentProps } from "../../store/commonTypes";
import React from "react";
import { Layout, Menu, Icon } from "antd";
import { sidebarItems } from "store/constants";
import { Link } from "react-router-dom";

const { Footer, Sider, Content } = Layout;

export class DefaultLayout extends React.Component<StandardComponentProps> {
  state = {
    collapsed: false
  };

  onCollapse = (collapsed: any) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };
  onSelect(val: any) {
    console.log(val);
  }

  render() {
    const { collapsed } = this.state;
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu
            theme="dark"
            defaultSelectedKeys={["0menu"]}
            onSelect={this.onSelect}
            mode="inline"
          >
            {sidebarItems.map((el, index) => {
              return (
                <Menu.Item key={index + "menu"}>
                  <Link to={el.linkTo}>
                    <Icon type="book" />
                    <span>{el.name}</span>
                  </Link>
                </Menu.Item>
              );
            })}
          </Menu>
        </Sider>
        <Layout>
          <Content style={{ margin: "0 16px" }}>{this.props.children}</Content>
          <Footer style={{ textAlign: "center" }}>
            Ilia Krasov & Igor Gorbonof ©2018
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
