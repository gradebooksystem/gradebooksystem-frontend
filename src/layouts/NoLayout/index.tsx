import React from "react";
import { StandardComponentProps } from "../../store/commonTypes";

export class NoLayout extends React.Component<StandardComponentProps> {
  render() {
    return this.props.children;
  }
}
