import React from "react";
import { Redirect } from "react-router-dom";
import { NoLayout } from "./layouts/NoLayout";
import { DefaultLayout } from "./layouts/DefaultLayout";
import RecordBookTable from "containers/recordbooks/table";
import RecordBookForm from "containers/recordbooks/form";
import fileImport from "components/fileImport";

export default [
  {
    path: "/",
    exact: true,
    layout: NoLayout,
    component: () => <Redirect to="/recordbooks" />
  },
  {
    path: "/recordbooks",
    exact: true,
    layout: DefaultLayout,
    component: RecordBookTable
  },
  {
    path: "/recordbooks/:actionOrId?",
    exact: true,
    layout: DefaultLayout,
    component: RecordBookForm
  },
  {
    path: "/file-import",
    exact: true,
    layout: DefaultLayout,
    component: fileImport
  }
];
