import React from "react";

import "./App.css";
import { history } from "helpers/history";
import { Router, Switch, Route } from "react-router-dom";
import { Result } from "antd";
import { Provider } from "react-redux";
import routes from "routes";
import { store } from "store";
const App: React.FC = () => {
  console.log("STORE", store);
  return (
    <Router history={history}>
      <Provider store={store}>
        <Switch>
          {routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={(props: any) => {
                  return (
                    <route.layout {...props}>
                      <route.component {...props} />
                    </route.layout>
                  );
                }}
              />
            );
          })}
          <Result
            status="404"
            title="404"
            subTitle="Sorry, the page you visited does not exist."
          />
        </Switch>
      </Provider>
    </Router>
  );
};

export default App;
