import { RecordBookTable } from "components/recordbooks/recordbooksTable";
import { connect } from "react-redux";
import { getRecordBooks } from "store/recordbooks/actions";

const mapStateToProps = (state: any) => {
  const { recordbooks } = state;
  return {
    values: recordbooks.values,
    loading: recordbooks.loading
  };
};

const mapDispatchToProps = {
  getRecordBooks: getRecordBooks
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordBookTable);
