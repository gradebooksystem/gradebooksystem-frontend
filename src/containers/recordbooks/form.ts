import RecordBookForm from "components/recordbooks/recodbooksForm";
import { connect } from "react-redux";
import {
  getRecordBook,
  setCurrentRecordBook,
  createRecordBook
} from "store/recordbooks/actions";
import { getFormData } from "store/common/actions";

const mapStateToProps = (state: any) => {
  const { recordbooks, common } = state;
  return {
    current: recordbooks.current,
    loading: recordbooks.loading,
    data: common
  };
};

const mapDispatchToProps = {
  create: createRecordBook,
  update: createRecordBook,
  getRecordBook: getRecordBook,
  setCurrentBook: setCurrentRecordBook,
  getFormData: getFormData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordBookForm);
